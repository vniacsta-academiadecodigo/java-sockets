package org.academiadecodigo.asynctomatics.simplewebserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {

    // fields to be used on the cleanup method
    public static final String ROOT = "www/";
    private DataOutputStream output;

    // constructor with the accept socket method
    // also dealing with exceptions
    public TCPServer(int portNumber) {

        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Ready to connect...");

            while (true) {
                Socket socket = serverSocket.accept();

                // initialize output stream
                output = new DataOutputStream(socket.getOutputStream());

                handleClient(socket);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // will handle client's request and answer
    private void handleClient(Socket socket) throws IOException {

        BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        String line = "";
        line = input.readLine();
        System.out.println("\nClient's request: " + line);

        String[] request = line.split(" ");

        // if the http verb is not GET, display error message
        if (!request[0].equals("GET")) {
            display(output, HttpHelper.notFound());
            socket.close();
        }

        // depending on the resource, display:
        if (request[1].equals("/vania")) {
            String resource = ROOT + "index.html";
            displayWebsite(resource);
        }

        if (request[1].equals("/vania.jpeg")) {
            String resource = ROOT + request[1];
            displayWebsite(resource);

        } else {
            display(output, HttpHelper.notFound());
            socket.close();
        }
    }

    private void displayWebsite(String resource) throws IOException {

        File file = new File(resource);

        display(output, HttpHelper.ok());
        display(output, HttpHelper.contentType(resource));
        display(output, HttpHelper.contentLength((int) file.length()));

        byte[] buffer = new byte[2048];
        FileInputStream in = new FileInputStream(file);

        int numBytes;
        while ((numBytes = in.read(buffer)) != -1) {
            output.write(buffer, 0, numBytes);
        }

        in.close();
    }

    private void display(DataOutputStream output, String response) throws IOException {
        output.writeBytes(response);
    }
}
