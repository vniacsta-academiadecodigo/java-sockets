package org.academiadecodigo.asynctomatics.simplewebserver;

public class Main {

    public static void main(String[] args) {

        int portNumber = 8083;

        try {
            new TCPServer(portNumber);

        } catch (NumberFormatException ex) {
            System.err.println("Invalid port number: " + portNumber);
        }

    }
}
