package org.academiadecodigo.asynctomatics.concurrentwebserver;

public class Main {

    public static void main(String[] args) {

        int portNumber = 8084;

        try {
            new ConcurrentServer(portNumber);

        } catch (NumberFormatException ex) {
            System.err.println("Invalid port number: " + portNumber);
        }
    }
}
