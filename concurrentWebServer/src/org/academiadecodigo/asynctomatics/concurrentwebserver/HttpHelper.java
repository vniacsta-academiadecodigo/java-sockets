package org.academiadecodigo.asynctomatics.concurrentwebserver;

public class HttpHelper {

    public static String notFound() {
        return "HTTP/1.1 404 Not Found\r\n";
    }

    public static String ok() {
        return "HTTP/1.1 200 Document Follows\r\n";
    }

    public static String contentLength(int length) {
        return "Content-Length: " + length + "\r\n\r\n";
    }

    public static String contentType(String file) {

        String extension = file.substring(file.lastIndexOf(".") + 1);

        switch (extension) {
            case "jpeg":
                return "Content-Type: image/jpeg\r\n";
            case "ico":
                return "Content-Type: image/ico\r\n";
            default:
                return "Content-Type: text/html; charset=UTF-8\r\n";
        }
    }
}
