package org.academiadecodigo.asynctomatics.concurrentwebserver;

import java.io.*;
import java.net.ServerSocket;

public class ConcurrentServer {

    // constructor with the accept socket method
    // also dealing with exceptions
    public ConcurrentServer(int portNumber) {

        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Ready to connect...");

            while (true) {
                ClientHandler clientHandler = new ClientHandler(serverSocket.accept());

                Thread thread = new Thread(clientHandler);
                thread.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
