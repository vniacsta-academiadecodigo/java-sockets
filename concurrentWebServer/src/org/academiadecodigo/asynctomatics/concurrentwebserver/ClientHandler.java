package org.academiadecodigo.asynctomatics.concurrentwebserver;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {

    public static final String ROOT = "www/";
    private Socket socket;
    private DataOutputStream output;

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            output = new DataOutputStream(socket.getOutputStream());
            handleClient(socket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // will handle client's request and answer
    private void handleClient(Socket socket) throws IOException {

        BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        String line = "";
        line = input.readLine();
        System.out.println("\nClient's request: " + line);

        String[] request = line.split(" ");

        // if the http verb is not GET, display error message
        if (!request[0].equals("GET")) {
            display(output, HttpHelper.notFound());
            socket.close();
        }

        // depending on the resource, display:
        if (request[1].equals("/vania")) {
            String resource = ROOT + "index.html";
            displayWebsite(resource);
        }

        if (request[1].equals("/vania.jpeg")) {
            String resource = ROOT + request[1];
            displayWebsite(resource);

        } else {
            display(output, HttpHelper.notFound());
            socket.close();
        }
    }

    private void displayWebsite(String resource) throws IOException {

        File file = new File(resource);

        display(output, HttpHelper.ok());
        display(output, HttpHelper.contentType(resource));
        display(output, HttpHelper.contentLength((int) file.length()));

        byte[] buffer = new byte[1024];
        FileInputStream in = new FileInputStream(file);

        int numBytes;
        while ((numBytes = in.read(buffer)) != -1) {
            output.write(buffer, 0, numBytes);
        }

        in.close();
    }

    private void display(DataOutputStream output, String response) throws IOException {
        output.writeBytes(response);
    }
}
