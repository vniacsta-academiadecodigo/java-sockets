package org.academiadecodigo.asynctomatics.concurrentchat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {

    // define port number
    public static int portNumber = 8080;

    private Socket clientSocket;
    private ServerSocket serverSocket;


    // main thread
    public static void main(String[] args) {

        ChatServer tcpServer = new ChatServer();
        tcpServer.start();
    }


    public void start() {

        try {
            execute();

        } catch (IOException ex) {
            System.err.println("Error: Network failure: " + ex.getMessage());

        } finally {
            cleanUp();
        }
    }

    private void execute() throws IOException {

        serverSocket = new ServerSocket(portNumber);
        System.out.println("Prepared for connection...");

        ClientHandler clientHandler = new ClientHandler(serverSocket.accept());
        Thread thread = new Thread(clientHandler);
        thread.start();

    }

    private void cleanUp() {

        try {
            if (clientSocket != null) {
                System.out.println("Closing the connection...");
                clientSocket.close();
            }

            if (serverSocket != null) {
                System.out.println("Closing the server socket...");
                serverSocket.close();
            }

        } catch (IOException ex) {
            System.err.println("Error: Network Failure: " + ex.getMessage());
        }
    }
}
