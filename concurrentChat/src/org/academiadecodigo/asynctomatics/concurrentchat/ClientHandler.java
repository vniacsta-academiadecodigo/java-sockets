package org.academiadecodigo.asynctomatics.concurrentchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientHandler implements Runnable {

    // fields
    private Socket clientSocket;

    // constructor
    public ClientHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {

        System.out.println("Connection established to port number " + ChatServer.portNumber);

        try {

            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            // saves info in a string
            String line = "";

            while (true) {
                // reads the line from the client
                line = input.readLine();
                System.out.println("Receiving: " + line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
