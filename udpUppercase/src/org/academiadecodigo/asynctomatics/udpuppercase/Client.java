package org.academiadecodigo.asynctomatics.udpuppercase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class Client {

    // fields
    private String hostName;
    private int portNumber;
    private String message;
    private DatagramSocket clientSocket;

    // method called in main
    public void start() {

        try {
            getUserInput();
            sendData();
            receiveData();

        } catch (SocketTimeoutException ex) {
            System.err.println("Error: Timeout Occurred, packet assumed lost");

        } catch (NumberFormatException ex) {
            System.err.println("Error: Invalid port!");

        } catch (SocketException ex) {
            System.err.println("Error: Could not connect to server: " + this.hostName + ':' + this.portNumber);

        } catch (UnknownHostException ex) {
            System.err.println("Error: Invalid host: " + this.hostName);

        } catch (IOException ex) {
            System.err.println("Error: Network Failure: " + ex.getMessage());

        } finally {
            clientSocket.close();
        }

    }

    private void getUserInput() throws IOException {

        // stream to read input from the terminal
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        // request to the terminal and save the input
        System.out.print("Hostname: ");
        hostName = input.readLine();

        // get the port number from the input and save it
        System.out.print("Port: ");
        portNumber = Integer.parseInt(input.readLine());

        // get the message from the input and save it
        System.out.print("Message: ");
        message = input.readLine();

        // close the stream
        input.close();
    }

    private void sendData() throws IOException {

        // create client socket
        clientSocket = new DatagramSocket();

        // get the host name with interface
        InetAddress address = InetAddress.getByName(hostName);

        // create send buffer and add the message
        byte[] sendBuffer = message.getBytes();

        // create datagram packet to send the message
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, address, portNumber);
        clientSocket.send(sendPacket);
    }

    private void receiveData() throws IOException {

        // create receive buffer
        byte[] receiveBuffer = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

        // sout for waiting and setting a timeout
        System.out.println("Waiting for return packet...");
        clientSocket.setSoTimeout(2000);

        // receiving the return message
        clientSocket.receive(receivePacket);
        String infoReceived = new String(receivePacket.getData(), 0, receivePacket.getLength());
        System.out.println("Received message: " + infoReceived);
    }
}
