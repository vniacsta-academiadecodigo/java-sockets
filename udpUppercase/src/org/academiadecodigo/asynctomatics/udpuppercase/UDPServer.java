package org.academiadecodigo.asynctomatics.udpuppercase;

import java.io.IOException;
import java.net.SocketException;

public class UDPServer {

    public static void main(String[] args) {

        try {
            // instantiate the class and the methods below
            Server udpServer = new Server();
            udpServer.getUserInput();
            udpServer.start();

        } catch (NumberFormatException ex) {
            System.err.println("Error: Invalid port!");

        } catch (SocketException ex) {
            System.err.println("Error: Could not accept incoming packets: " + ex.getMessage());

        } catch (IOException ex) {
            System.err.println("Error: Network failure: " + ex.getMessage());
        }
    }
}
