package org.academiadecodigo.asynctomatics.udpuppercase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class Server {

    // port number
    private int portNumber;

    // method called in main
    public void start() throws IOException {

        DatagramSocket serverSocket = new DatagramSocket(portNumber);
        System.out.println("Listening to connections...");

        // create receive buffer to listen to possible connections
        byte[] receiveBuffer = new byte[1024];

        // while the connection is on, the socket will receive
        while (serverSocket.isBound()) {

            // create and RECEIVE UDP datagram packet from the socket
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            System.out.println("Waiting for packet...");
            serverSocket.receive(receivePacket);

            // save message received and transform it to uppercase
            String infoReceived = new String(receivePacket.getData(), 0,
                    receivePacket.getLength()).toUpperCase();

            // get address and port from the received packet
            InetAddress address = receivePacket.getAddress();
            int portNumber = receivePacket.getPort();

            // create buffer to send the data received
            byte[] sendBuffer = infoReceived.getBytes();

            // create and SEND UDP datagram packet from the socket
            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, address, portNumber);
            serverSocket.send(sendPacket);
        }
    }

    public void getUserInput() throws IOException {

        // create a bufferedreader stream with the design pattern decorator
        // system in will get the input from the terminal
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        // print instead of println won't create a paragraph
        System.out.print("Port: ");
        // interger.parseint will convert the string received from the input into integer
        portNumber = Integer.parseInt(input.readLine());

        // close the stream
        input.close();
    }
}
