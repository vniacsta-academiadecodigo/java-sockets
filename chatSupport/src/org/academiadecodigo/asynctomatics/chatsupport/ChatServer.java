package org.academiadecodigo.asynctomatics.chatsupport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {

    // define port number
    private int portNumber = 8080;

    private Socket clientSocket;
    private ServerSocket serverSocket;
    private BufferedReader input;

    public static void main(String[] args) {

        ChatServer tcpServer = new ChatServer();
        tcpServer.start();
    }

    public void start() {

        try {
            execute();

        } catch (IOException ex) {
            System.err.println("Error: Network failure: " + ex.getMessage());

        } finally {
            cleanUp();
        }
    }

    private void execute() throws IOException {

        serverSocket = new ServerSocket(portNumber);
        System.out.println("Prepared for connection...");
        clientSocket = serverSocket.accept();
        System.out.println("Connection established to port number " + portNumber);

        input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        // saves info in a string
        String line = "";

        while (true) {
            // reads the line from the client
            line = input.readLine();
            System.out.println("Receiving: " + line);
        }
    }

    private void cleanUp() {

        try {
            if (clientSocket != null) {
                System.out.println("Closing the connection...");
                clientSocket.close();
            }

            if (serverSocket != null) {
                System.out.println("Closing the server socket...");
                serverSocket.close();
            }

        } catch (IOException ex) {
            System.err.println("Error: Network Failure: " + ex.getMessage());
        }
    }
}
