package org.academiadecodigo.asynctomatics.chatsupport;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient {

    // fields
    private String hostName = "localhost";
    private int portNumber = 8080;
    private Socket clientSocket;
    private BufferedWriter output;
    private BufferedReader input;

    public static void main(String[] args) {

        ChatClient chatClient = new ChatClient();
        chatClient.start();
    }

    private void start() {

        try {
            execute();

        } catch (UnknownHostException ex) {
            System.err.println("Error: Invalid host: " + this.hostName);

        } catch (IOException ex) {
            System.err.println("Error: Network Failure: " + ex.getMessage());

        } finally {
            cleanUp();
        }
    }

    private void execute() throws IOException {

        InetAddress address = InetAddress.getByName(hostName);

        clientSocket = new Socket(address, portNumber);
        System.out.println("Connected to server!");

        // takes input and output
        input = new BufferedReader(new InputStreamReader(System.in));
        output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

        // saves info in a string
        String line = "";

        while (true) {
            // reads the line
            line = input.readLine();

            // writes with a new line and flushes to make sure all info is transmitted
            output.write(line);
            output.newLine();
            output.flush();
        }
    }

    private void cleanUp() {

        try {
            if (clientSocket != null) {
                System.out.println("Closing the connection...");
                // not needed to close the other connections as they are connected to this one
                // and all of them will be closed
                clientSocket.close();
            }

        } catch (IOException ex) {
            System.err.println("Error: Network Failure: " + ex.getMessage());
        }
    }
}
